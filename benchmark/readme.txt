
comb1000
    sequential chain of 1000 adders
    input = 1 => output = 1
    input = 0 => output = 0

comb10000
    sequential chain of 10000 adders
    input = 1 => output = 1
    input = 0 => output = 0

nappes
    if i = 0 then
		0101...01
	       +1010...10
	       ----------
		1111...11
	       +0000...00
	       +        1
	       ----------
	       10000...00
	      /  \\\\\\\\\
	  c2=1    serial disjunction -> oo = 0

    else i = 1 then
		1010...10
	       +0101...01
	       ----------
		1111...11
	       +0000...00
	       +        0
	       ----------
	       01111...11
	      /  \\\\\\\\\
	  c2=0    serial disjunction -> oo = 1
    endif

seq0
    32-bit input counter yielding only the final carry flag.
    no nappes

seq
    32-bit input counter yielding the result
    nappes

results with 'official' netlist_simulater.native:
    comb1000:  1.15s
    comb10000: 3.27s
    nappes:    2.14s
    seq0:      0.63s
    seq:       1.01s

