TIME=/usr/bin/time
SIM=${1:-../netlist_sim.byte}

#while true; do echo 1; echo 0; done | \
#    $TIME -f 'comb1000:  %es' $SIM -steps 100 -net ./comb1000.net > /dev/null

#while true; do echo 1; echo 0; done | \
#    $TIME -f 'comb10000: %es' $SIM -steps 20 -net ./comb10000.net > /dev/null

#while true; do echo 1; echo 0; done | \
#    $TIME -f 'nappes:    %es' $SIM -steps 10 -net ./nappes.net > /dev/null

while true; do echo 1; done | \
    $TIME -f 'seq0:      %es' $SIM -steps 8192 -net ./seq0.net > /dev/null

while true; do echo 1; done | \
    $TIME -f 'seq:       %es' $SIM -steps 8192 -net ./seq.net > /dev/null

